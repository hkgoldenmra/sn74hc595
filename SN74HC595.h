#ifndef SN74HC595_H
#define SN74HC595_H

#include <Arduino.h>

class SN74HC595 {
	private:
		byte sdiPin;
		byte lckPin;
		byte sckPin;
	public:
		SN74HC595(byte, byte, byte);
		void initial();
		void run(byte);
};

#endif