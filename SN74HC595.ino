#include "SN74HC595.h"

SN74HC595 ic = SN74HC595(A2, A1, A0);
byte i = 0;

void setup() {
	Serial.begin(115200);
	ic.initial();
}

void loop() {
	ic.run(i++); // anode output
//	ic.run(0xff ^ i++); // cathode output
	delay(500);
}