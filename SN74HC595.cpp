#include "SN74HC595.h"

SN74HC595::SN74HC595(byte sdiPin, byte lckPin, byte sckPin) {
	this->sdiPin = sdiPin;
	this->lckPin = lckPin;
	this->sckPin = sckPin;
}

void SN74HC595::initial() {
	pinMode(this->sdiPin, OUTPUT);
	digitalWrite(this->sdiPin, HIGH);
	pinMode(this->lckPin, OUTPUT);
	digitalWrite(this->lckPin, HIGH);
	pinMode(this->sckPin, OUTPUT);
}

void SN74HC595::run(byte data) {
	digitalWrite(this->lckPin, LOW);
	for (byte i = 0; i < 8; i++) {
		digitalWrite(this->sckPin, LOW);
		if ((data & (1 << i)) > 0) {
			digitalWrite(this->sdiPin, HIGH);
		} else {
			digitalWrite(this->sdiPin, LOW);
		}
		digitalWrite(this->sckPin, HIGH);
	}
	digitalWrite(this->lckPin, HIGH);
}